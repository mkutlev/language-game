# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/mariok/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# EventBus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

# Removes the logs
-assumenosideeffects class com.okotta.util.AppLogger {
    public static void v(...);
    public static void e(...);
}

-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** e(...);
}

# ButterKnife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.okotta.model.** { *; }
-keep class com.okotta.*.model.** { *; }
-keep class com.okotta.communication.request.** { *; }
-keep class com.okotta.communication.response.** { *; }

##---------------End: proguard configuration for Gson  ----------

# Retrolambda
-dontwarn java.lang.invoke.*

# Lombok
-keep class java.beans.ConstructorProperties { *; }
-keepattributes ConstructorProperties
-dontwarn java.beans.ConstructorProperties

# Retrofit
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions