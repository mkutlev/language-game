package mk.language.app;

import android.app.Application;
import android.content.Context;

import mk.language.app.injection.component.ApplicationComponent;
import mk.language.app.injection.component.DaggerApplicationComponent;
import mk.language.app.injection.module.ApplicationModule;

public class LangGameApplication extends Application {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static LangGameApplication get(Context context) {
        return (LangGameApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (applicationComponent == null) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return applicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }

}
