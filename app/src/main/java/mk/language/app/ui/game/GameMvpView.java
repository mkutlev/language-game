package mk.language.app.ui.game;

import mk.language.app.data.model.WordGame;
import mk.language.app.ui.base.MvpView;

public interface GameMvpView extends MvpView {

/*
    void showCorrectAnswerCount(int count, int successCount);

    void showWrongAnswerCount(int count);
*/

    void showLoading(boolean show);

    void startGame(WordGame wordGame);

//    void showWord(String word, String translation);

/*    void showSuccessEnd();

    void showNotSuccessEnd();*/

}
