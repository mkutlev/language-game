package mk.language.app.ui.game;

import java.util.Random;

import javax.inject.Inject;

import mk.language.app.data.DataManager;
import mk.language.app.data.model.WordGame;
import mk.language.app.ui.base.BasePresenter;
import mk.language.app.util.AppLogger;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class GamePresenter extends BasePresenter<GameMvpView> {

    // TODO
    private DataManager dataManager;

    private Subscription subscription;

    protected WordGame wordGame;

    private Random random;
    private boolean currentTranslationCorrect;

    @Inject
    public GamePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(GameMvpView mvpView) {
        super.attachView(mvpView);

        if (wordGame == null) {
            loadGameData();
        } else {
            startGame();
        }
    }

    @Override
    public void detachView() {
        super.detachView();

        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    private void loadGameData() {
        getMvpView().showLoading(true);

        subscription = dataManager.getWordGame()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<WordGame>() {
                    @Override
                    public void onCompleted() {
                        AppLogger.d(TAG, "Game words loaded!");

                        if (isViewAttached()) {
                            getMvpView().showLoading(false);
                            startGame();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        AppLogger.e(TAG, "Error loading words!");
                    }

                    @Override
                    public void onNext(WordGame wordGame) {
                        GamePresenter.this.wordGame = wordGame;
                    }
                });
    }

    protected void startGame() {
        random = new Random();

        getMvpView().startGame(wordGame);

//        showNextWord();
    }

    private void showNextWord() {
        String nextWord = wordGame.getNextWord();

        currentTranslationCorrect = random.nextBoolean();
        String translation = currentTranslationCorrect ?
                wordGame.getCorrectTranslation() : wordGame.getWrongTranslation();

//        getMvpView().showWord(nextWord, translation);
    }

    public void handleAnswer(boolean translationCorrect) {
        if (translationCorrect == currentTranslationCorrect) {
            // Correct answer

        } else {
            // Wrong answer

        }
    }

}
