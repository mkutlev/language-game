package mk.language.app.ui.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import mk.language.app.LangGameApplication;
import mk.language.app.injection.component.ActivityComponent;
import mk.language.app.injection.component.DaggerActivityComponent;
import mk.language.app.injection.module.ActivityModule;
import mk.language.app.util.AppLogger;

public class BaseActivity extends AppCompatActivity {

    protected final String TAG = getClass().getSimpleName();

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppLogger.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null) {

            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(LangGameApplication.get(this).getComponent())
                    .build();
        }
        return activityComponent;
    }

}
