package mk.language.app.ui.game;

import android.os.Bundle;

import javax.inject.Inject;

import butterknife.ButterKnife;
import lombok.val;
import mk.language.app.R;
import mk.language.app.data.model.WordGame;
import mk.language.app.ui.base.BaseActivity;

public class GameActivity extends BaseActivity implements GameMvpView {

    @Inject
    GamePresenter gamePresenter;

//    @BindView(R.id.login_email)
//    EditText emailEditText;
//    @BindView(R.id.login_password)
//    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Set up activity theme
        // Make sure this is before calling super.onCreate
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getActivityComponent().inject(this);
        ButterKnife.bind(this);
        gamePresenter.attachView(this);

        val actionBar = getSupportActionBar();
        if (actionBar != null) {
//            actionBar.setTitle(R.string.loginTitle);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gamePresenter.detachView();
    }

    @Override
    public void showLoading(boolean show) {
        // TODO
    }

    @Override
    public void startGame(WordGame wordGame) {

    }

}
