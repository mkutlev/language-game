package mk.language.app.event;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserEvent {

    public enum Type {
        LOG_IN, LOG_OUT, USER_UPDATE, ACCOUNT_CREATED
    }

    private Type type;

}
