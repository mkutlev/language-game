package mk.language.app.injection.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mk.language.app.LangGameApplication;
import mk.language.app.injection.ApplicationContext;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {

    protected final LangGameApplication application;

    public ApplicationModule(LangGameApplication application) {
        this.application = application;
    }

    @Provides
    LangGameApplication provideApplication() {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

}
