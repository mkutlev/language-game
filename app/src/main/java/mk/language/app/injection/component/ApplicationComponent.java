package mk.language.app.injection.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import mk.language.app.LangGameApplication;
import mk.language.app.data.DataManager;
import mk.language.app.injection.ApplicationContext;
import mk.language.app.injection.module.ApplicationModule;
import mk.language.app.manager.EventManager;
import mk.language.app.manager.SharedPreferencesManager;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context getContext();

    LangGameApplication getApplication();

    DataManager getDataManager();

    EventManager getEventManager();

    SharedPreferencesManager getSharedPreferencesManager();

}
