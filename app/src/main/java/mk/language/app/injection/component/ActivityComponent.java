package mk.language.app.injection.component;

import dagger.Component;
import mk.language.app.injection.PerActivity;
import mk.language.app.injection.module.ActivityModule;
import mk.language.app.ui.game.GameActivity;
import mk.language.app.ui.main.MainActivity;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(GameActivity gameActivity);

}
