package mk.language.app.util;

import timber.log.Timber;

/**
 * Logging library abstraction
 */
public final class AppLogger {

    private AppLogger() {
    }

    public static void d(String tag, String message) {
        Timber.d(tag, message);
    }

    public static void e(String tag, String message) {
        Timber.e(tag, message);
    }

    public static void e(String tag, String message, Throwable tr) {
        Timber.e(tag, message, tr);
    }

}
