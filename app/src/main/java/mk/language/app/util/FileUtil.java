package mk.language.app.util;

import android.content.Context;
import android.support.annotation.WorkerThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileUtil {

    private static final String TAG = FileUtil.class.getSimpleName();

    private FileUtil() {
    }

    @WorkerThread
    public static String readAssetFile(Context context, String fileName) {
        BufferedReader reader = null;
        try {
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(fileName)));

            String line = reader.readLine();
            while (line != null) {
                sb.append(line).append("\n");
                line = reader.readLine();
            }
            return sb.toString();
        } catch (IOException e) {
            AppLogger.e(TAG, "readAssetFile", e);
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    AppLogger.e(TAG, "readAssetFile - close exception", e);
                }
            }
        }
    }

}
