package mk.language.app.util;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtil {

    private static final GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting();

    @NonNull
    public static Gson getGson() {
        return gsonBuilder.create();
    }

}