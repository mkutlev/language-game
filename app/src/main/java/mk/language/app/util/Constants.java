package mk.language.app.util;

/**
 * Here will be stored project's global constants.
 */
public final class Constants {

    /**
     * All fields of this class should be constants.
     */
    private Constants() {
    }

    // Word Game constants
    /**
     * Initial time of the game
     */
/*    public static final long WORD_GAME_START_TIME = 60 * 1000;
    *//**
     * Time to add in case of correct answer
     *//*
    public static final long WORD_GAME_CORRECT_ANSWER_TIME = 15 * 1000;
    *//**
     * Time to remove in case of wrong answer
     *//*
    public static final long WORD_GAME_WRONT_ANSWER_TIME = 5 * 1000;

    */

    /**
     * Correct answers needed to finish the game successfully
     */
    public static final int WORD_GAME_CORRECT_ANSWERS_TO_SUCCEED = 15;

    /**
     * Maximum mistakes allowed
     */
    public static final int WORD_GAME_MAX_WRONG_ANSWERS = 5;

    /**
     * Time to answer
     */
    public static final long WORD_GAME_TIME_TO_ANSWER = 15 * 1000;

}
