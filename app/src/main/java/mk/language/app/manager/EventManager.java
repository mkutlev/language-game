package mk.language.app.manager;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

// EventBus documentation -> http://greenrobot.org/eventbus
@Singleton
public final class EventManager {

    private EventBus eventBus;

    @Inject
    public EventManager() {
        eventBus = EventBus.getDefault();
    }

    /**
     * Registers the given subscriber to receive events.
     */
    public void register(Object subscriber) {
        eventBus.register(subscriber);
    }

    /**
     * Unregisters the given subscriber from all event classes.
     */
    public void unregister(Object subscriber) {
        eventBus.unregister(subscriber);
    }

    public boolean isRegistered(Object subscriber) {
        return eventBus.isRegistered(subscriber);
    }

    public void postEvent(Object event) {
        eventBus.post(event);
    }

    public void cancelEventDelivery(@NonNull Object event) {
        eventBus.cancelEventDelivery(event);
    }

}
