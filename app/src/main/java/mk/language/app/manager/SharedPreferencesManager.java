package mk.language.app.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

import mk.language.app.LangGameApplication;
import mk.language.app.util.AppLogger;

@Singleton
public final class SharedPreferencesManager {

    private static final String TAG = SharedPreferencesManager.class.getSimpleName();
    private static final String PREFS_NAME = LangGameApplication.class.getPackage() + "_shared_prefs";

    private SharedPreferences sharedPreferences;
    private Gson gson;

    @Inject
    public SharedPreferencesManager(LangGameApplication application) {
        sharedPreferences = application.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    /**
     * Checks whether the preferences contains a preference.
     *
     * @param key The name of the preference to check.
     * @return Returns true if the preference exists in the preferences, otherwise false.
     */
    public boolean contains(String key) {
        return sharedPreferences.contains(key);
    }

    /**
     * Saves an integer value for the given key in the shared preferences.
     */
    public void putInt(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    /**
     * Returns the integer value for the given key from the shared preferences.
     */
    public int getInt(String key, int defValue) {
        try {
            return sharedPreferences.getInt(key, defValue);
        } catch (ClassCastException ex) {
            AppLogger.e(TAG, "The preference with name \"" + key + "\" is NOT int type!", ex);
        }
        return defValue;
    }

    /**
     * Saves a string value for the given key in the shared preferences.
     */
    public void putString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * Returns the string value for the given key from the shared preferences.
     */
    public String getString(String key, String defValue) {
        try {
            return sharedPreferences.getString(key, defValue);
        } catch (Exception ex) {
            AppLogger.e(TAG, "The preference with name \"" + key + "\" is NOT String type!", ex);
        }
        return defValue;
    }

    /**
     * Saves a boolean value for the given key in the shared preferences.
     */
    public void putBoolean(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    /**
     * Returns the boolean value for the given key from the shared preferences.
     */
    public boolean getBoolean(String key, boolean defValue) {
        try {
            return sharedPreferences.getBoolean(key, defValue);
        } catch (ClassCastException ex) {
            AppLogger.e(TAG, "The preference with name \"" + key + "\" is NOT String type!", ex);
        }
        return defValue;
    }

    public void remove(String key) {
        sharedPreferences.edit().remove(key).apply();
    }

    public SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    public void putObject(String key, Object object) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        String json = gson.toJson(object);
        prefsEditor.putString(key, json);
        prefsEditor.apply();
    }

    public void putObject(final String key, final Object object, boolean asynchronously) {
        if (asynchronously) {
            new Thread(() -> {
                putObject(key, object);
            }).start();
        } else {
            putObject(key, object);
        }
    }

    public <T> T getObject(String key, Class<T> clazz) {
        String json = sharedPreferences.getString(key, null);
        if (json == null) {
            return null;
        }

        return gson.fromJson(json, clazz);
    }

}
