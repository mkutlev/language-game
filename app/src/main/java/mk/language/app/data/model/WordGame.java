package mk.language.app.data.model;

import java.util.List;
import java.util.Random;

public class WordGame {

    private List<Word> words;

    private int currentWord;

    private Random randomGenerator;

    public WordGame(List<Word> words) {
        this.words = words;

        currentWord = -1;
        randomGenerator = new Random();
    }

    public boolean hasMoreWords() {
        if (words == null || words.size() == 0 || currentWord >= words.size() - 1) {
            return false;
        }

        return true;
    }

    public String getNextWord() {
        currentWord++;

        return words.get(currentWord).getValue();
    }

    public String getCorrectTranslation() {
        return words.get(currentWord).getTranslation();
    }

    public String getWrongTranslation() {
        int randomWordIndex;
        do {
            randomWordIndex = getRandomNumber(0, words.size());
        } while (randomWordIndex == currentWord);

        return words.get(randomWordIndex).getTranslation();
    }

    /**
     * Returns random number in the range
     */
    private int getRandomNumber(int start, int end) {
        return randomGenerator.nextInt(end - start) + start;
    }

}
