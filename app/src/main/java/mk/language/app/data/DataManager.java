package mk.language.app.data;

import android.support.annotation.WorkerThread;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import mk.language.app.LangGameApplication;
import mk.language.app.data.model.Word;
import mk.language.app.data.model.WordGame;
import mk.language.app.util.AppLogger;
import mk.language.app.util.FileUtil;
import mk.language.app.util.GsonUtil;
import rx.Observable;
import rx.Subscriber;

@Singleton
public class DataManager {

    private static final String TAG = DataManager.class.getSimpleName();

    private static final String WORDS_FILE_NAME = "words.json";

    private LangGameApplication application;

    @Inject
    public DataManager(LangGameApplication application) {
        this.application = application;

    }

    public Observable<WordGame> getWordGame() {
        return Observable.create(new Observable.OnSubscribe<WordGame>() {
            @Override
            public void call(Subscriber<? super WordGame> subscriber) {
                WordGame wordGame = new WordGame(getWordsFromAssets());
                subscriber.onNext(wordGame);
                subscriber.onCompleted();
            }
        });
    }

    /**
     * Reads the words from local asset file
     *
     * @return array of words
     */
    @WorkerThread
    private List<Word> getWordsFromAssets() {
        String wordsJson = FileUtil.readAssetFile(application, WORDS_FILE_NAME);
        if (wordsJson == null) {
            AppLogger.e(TAG, "getWordsFromAssets -> Could not read the file!");
            return null;
        }

        // Parse json
        Word[] words;
        try {
            words = GsonUtil.getGson().fromJson(wordsJson, Word[].class);
        } catch (Exception ex) {
            AppLogger.e(TAG, "Error parsing words json! fileName = " + WORDS_FILE_NAME);
            words = new Word[0];
        }

        List<Word> wordList = Arrays.asList(words);
        // Randomly rearrange the list
        Collections.shuffle(wordList);

        return wordList;
    }

}
