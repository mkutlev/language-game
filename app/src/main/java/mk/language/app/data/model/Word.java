package mk.language.app.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Word {

    /**
     * Spanish word
     */
    @SerializedName("text_spa")
    private String value;

    /**
     * English translation for the spanish word
     */
    @SerializedName("text_eng")
    private String translation;

}
