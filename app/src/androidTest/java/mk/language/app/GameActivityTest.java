package mk.language.app;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import mk.language.app.test.common.TestComponentRule;
import mk.language.app.ui.game.GameActivity;

@RunWith(AndroidJUnit4.class)
public class GameActivityTest {

    public final TestComponentRule component =
            new TestComponentRule(InstrumentationRegistry.getTargetContext());

    public final ActivityTestRule<GameActivity> languageGame =
            new ActivityTestRule<GameActivity>(GameActivity.class, false, false) {
                @Override
                protected Intent getActivityIntent() {
                    // Override the default intent so we pass a false flag for syncing so it doesn't
                    // start a sync service in the background that would affect  the behaviour of
                    // this test.
                    return new Intent(InstrumentationRegistry.getTargetContext(), GameActivity.class);
                }
            };

    // TestComponentRule needs to go first to make sure the Dagger ApplicationTestComponent is set
    // in the Application before any Activity is launched.
    @Rule
    public final TestRule chain = RuleChain.outerRule(component).around(languageGame);

    @Test
    public void testFields() {
        languageGame.launchActivity(null);

/*        String testEmail = "test@gmail.com";
        onView(withId(R.id.login_email)).check(matches(isDisplayed())).perform(click())
                .perform(typeTextIntoFocusedView(testEmail))
                .check(matches(withText(testEmail)));

        String testPassword = "123456";
        onView(withId(R.id.login_password)).check(matches(isDisplayed())).perform(click())
                .perform(typeTextIntoFocusedView(testPassword))
                .check(matches(withText(testPassword)));

        onView(withId(R.id.login_button)).check(matches(isDisplayed()));*/

    }

}