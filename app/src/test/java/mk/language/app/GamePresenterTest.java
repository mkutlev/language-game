package mk.language.app;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import mk.language.app.data.DataManager;
import mk.language.app.ui.game.GameMvpView;
import mk.language.app.ui.game.GamePresenter;
import mk.language.app.util.RxSchedulersOverrideRule;

//import mk.android.test.common.TestDataFactory;

@RunWith(MockitoJUnitRunner.class)
public class GamePresenterTest {

    @Mock
    GameMvpView mockLoginMvpView;

    @Mock
    DataManager dataManager;

    private GamePresenter gamePresenter;

    @Rule
    public final RxSchedulersOverrideRule overrideSchedulersRule = new RxSchedulersOverrideRule();

    @Before
    public void setUp() {
        gamePresenter = new GamePresenter(dataManager);
        gamePresenter.attachView(mockLoginMvpView);
    }

    @After
    public void tearDown() {
        gamePresenter.detachView();
    }

    // TODO

    @Test
    public void loginSuccess() throws IOException {
        /*String username = "test_user";
        LoginResponse loginResponse = TestDataFactory.makeLoginResponse(username);

        Response<LoginResponse> successResponse = Response.success(loginResponse);

        gamePresenter.login(username, "password");
        gamePresenter.onResponse(null, successResponse);

        verify(mockLoginMvpView).onLoginSuccessful(loginResponse.getUser());
        verify(mockLoginMvpView, never()).showLoginError();*/
    }

}