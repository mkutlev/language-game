package mk.language.app.test.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import mk.language.app.injection.component.ApplicationComponent;
import mk.language.app.test.common.injection.module.ApplicationTestModule;
import mk.language.app.ui.game.GamePresenter;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

    void inject(GamePresenter presenter);

}
