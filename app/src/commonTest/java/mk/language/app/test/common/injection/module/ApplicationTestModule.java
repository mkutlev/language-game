package mk.language.app.test.common.injection.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mk.language.app.LangGameApplication;
import mk.language.app.data.DataManager;
import mk.language.app.injection.ApplicationContext;

import static org.mockito.Mockito.mock;

/**
 * Provides application-level dependencies for an app running on a testing environment
 * This allows injecting mocks if necessary.
 */
@Module
public class ApplicationTestModule {

    protected final LangGameApplication application;

    public ApplicationTestModule(LangGameApplication application) {
        this.application = application;
    }

    @Provides
    LangGameApplication provideApplication() {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    /**
     * ************ MOCKS ************
     */

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return mock(DataManager.class);
    }

}
