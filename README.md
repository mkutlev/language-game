# Simple Android App #

### What is this repository for? ###

* Android application prototype of Falling word game that showcases MVP architecture and libraries used for base applications.

### How do I get set up? (Android Studio) ###

* Install [Lombok plugin](https://projectlombok.org/setup/android.html)
* Import the project

### Libraries and tools included ###

 - Support libraries
 - RxJava and RxAndroid
 - Dagger 2
 - Butterknife
 - Lombok